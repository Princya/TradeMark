package com.aeq.trademark;


import com.fasterxml.jackson.annotation.JsonProperty;

public class TradeMark {
	@JsonProperty("SerialNo")
	private String  sNo;
	
	@JsonProperty("ApplicationNo")
	private String applicationNo;
	
	@JsonProperty("ApplicationDate")
	private String applicationDate;
	
	@JsonProperty("Class")
	private String classValue;
	
	@JsonProperty("TradeMark")
	private String tradeMark;
	
	@JsonProperty("ReportDate")
	private String reportDate;
	
	@JsonProperty("Expedite")
	private String expedite;
	
	@JsonProperty("Status")
	private String status;

	public String getsNo() {
		return sNo;
	}

	public void setsNo(String sNo) {
		this.sNo = sNo;
	}

	public String getApplicationNo() {
		return applicationNo;
	}

	public void setApplicationNo(String applicationNo) {
		this.applicationNo = applicationNo;
	}

	public String getApplicationDate() {
		return applicationDate;
	}

	public void setApplicationDate(String applicationDate) {
		this.applicationDate = applicationDate;
	}

	public String getClassValue() {
		return classValue;
	}

	public void setClassValue(String classValue) {
		this.classValue = classValue;
	}

	public String getTradeMark() {
		return tradeMark;
	}

	public void setTradeMark(String tradeMark) {
		this.tradeMark = tradeMark;
	}

	public String getReportDate() {
		return reportDate;
	}

	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}

	public String getExpedite() {
		return expedite;
	}

	public void setExpedite(String expedite) {
		this.expedite = expedite;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/*@Override
	public String toString() {
		return "TradeMark [sNo=" + sNo + ", applicationNo=" + applicationNo + ", applicationDate=" + applicationDate
				+ ", classValue=" + classValue + ", tradeMark=" + tradeMark + ", reportDate=" + reportDate
				+ ", expedite=" + expedite + ", status=" + status + "]";
	}
	*/
	
	
}
